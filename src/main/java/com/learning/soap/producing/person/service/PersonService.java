package com.learning.soap.producing.person.service;

import com.learning.soap.producing.person.dto.PersonInformation;
import com.learning.soap.producing.person.model.Person;
import com.learning.soap.producing.person.repository.PersonRepository;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;


@Log
@Service
@AllArgsConstructor
public class PersonService {

    private PersonRepository personRepository;
    public List<Person> getPeople(){
        return personRepository.findAll();
    }

    public Person findPersonByPesel(@NonNull Integer pesel){
        Person person = personRepository.findPersonByPesel(pesel);
        if(person == null){
            log.warning("Couldn't find person with pesel: " + pesel);
            return null;
        }
        else return person;
    }

    public boolean deletePerson(Integer pesel) {
        Person person = personRepository.findPersonByPesel(pesel);
        if(person!=null){
            personRepository.delete(person);
            log.info("Deleted person with pesel: " + pesel);
            return true;
        }
        else
        {
            log.warning("Couldn't find person with pesel: " + pesel);
            return false;
        }
    }

    public boolean addPerson(PersonInformation personInformation) {
        Person person = personRepository.findPersonByPesel(personInformation.getPesel());
        if(person!=null){
            log.warning("Person with pesel: "+ personInformation.getPesel() + " already exists");
            return false;

        }
        else{
            person = Person.builder()
                    .name(personInformation.getName())
                    .surname(personInformation.getSurname())
                    .pesel(personInformation.getPesel())
                    .build();
            personRepository.save(person);
            return true;
        }
    }
}
