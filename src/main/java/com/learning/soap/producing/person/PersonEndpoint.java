package com.learning.soap.producing.person;

import com.learning.soap.producing.generated.*;
import com.learning.soap.producing.person.assembler.PersonAssembler;
import com.learning.soap.producing.person.dto.PersonInformation;
import com.learning.soap.producing.person.model.Person;
import com.learning.soap.producing.person.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
@RequiredArgsConstructor
public class PersonEndpoint {
    @Autowired
    private PersonService personService;

    @PayloadRoot(namespace = "http://learning/soap", localPart = "getPersonRequest")
    @ResponsePayload
    GetPersonResponse getPerson(@RequestPayload GetPersonRequest personRequest) {
        return PersonAssembler.personResponseToPerson(personService.findPersonByPesel(personRequest.getPesel()));
    }

    @PayloadRoot(namespace = "http://learning/soap", localPart = "getPeopleRequest")
    @ResponsePayload
    GetPeopleResponse getPeople(@RequestPayload GetPeopleRequest peopleRequest) {
        List<Person> people = personService.getPeople();
        return PersonAssembler.getPeopleAssebler(people);
    }


    @PayloadRoot(namespace = "http://learning/soap", localPart = "addPersonRequest")
    @ResponsePayload
    AddPersonResponse addPerson(@RequestPayload AddPersonRequest addPersonRequest) {
        PersonInformation personInformation = PersonAssembler.personWsToPersonInformation(addPersonRequest.getPersonWs());
        return PersonAssembler.addPersonAssembler(personService.addPerson(personInformation), addPersonRequest.getPersonWs());
    }

    @PayloadRoot(namespace = "http://learning/soap", localPart = "deletePersonRequest")
    @ResponsePayload
    DeletePersonResponse deletePerson(@RequestPayload DeletePersonRequest deletePersonRequest) {
        Integer pesel = deletePersonRequest.getPesel();
        return PersonAssembler.deletePersonAssembler(personService.deletePerson(pesel));
    }

}
