package com.learning.soap.producing.person.assembler;

import com.learning.soap.producing.generated.*;
import com.learning.soap.producing.person.dto.PersonInformation;
import com.learning.soap.producing.person.model.Person;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.util.ArrayList;
import java.util.List;

public class PersonAssembler {
    private static final ModelMapper mapper = new ModelMapper();

    static {
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    public static GetPersonResponse personResponseToPerson(Person person) {
        PersonWs personWs = mapper.map(person, PersonWs.class);
        return new GetPersonResponse(personWs);

    }

    public static GetPeopleResponse getPeopleAssebler(List<Person> people) {

        List<PersonWs> peopleWs = new ArrayList<>();

        for (Person person : people) {
            PersonWs personWs = PersonWs.builder()
                    .pesel(person.getPesel())
                    .name(person.getName())
                    .surname(person.getSurname())
                    .build();

            peopleWs.add(personWs);
        }

        return new GetPeopleResponse(peopleWs);
    }

    public static DeletePersonResponse deletePersonAssembler(Boolean ifDeleted) {
        Integer codeStatus;
        String desctiption;
        if (ifDeleted) {
            codeStatus = 200;
            desctiption = "Person deleted ";
        } else {
            codeStatus = 400;
            desctiption = "Person not deleted";
        }

        return new DeletePersonResponse(codeStatus, desctiption);

    }

    public static AddPersonResponse addPersonAssembler(Boolean ifAdded, PersonWs personWs) {
        Integer codeStatus;
        String desctiption;
        if (ifAdded) {
            codeStatus = 200;
            desctiption = "Person created ";
        } else {
            codeStatus = 400;
            desctiption = "Person not created";
        }

        return new AddPersonResponse(personWs, codeStatus, desctiption);
    }

    public static PersonInformation personWsToPersonInformation(PersonWs personWs) {
        PersonInformation personInformation = PersonInformation.builder()
                .pesel(personWs.getPesel())
                .name(personWs.getName())
                .surname(personWs.getSurname())
                .build();
        return personInformation;
    }
}
