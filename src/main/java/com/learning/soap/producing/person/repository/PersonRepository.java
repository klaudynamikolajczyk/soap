package com.learning.soap.producing.person.repository;

import com.learning.soap.producing.person.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository <Person, Integer> {
    Person findPersonByPesel(Integer pesel);
}
